import "./App.css";
import LoginModal from "./Tables/LoginModal";
import React from "react";
import DashBoard from "./Tables/Dashboard";
import Cookies from "js-cookie";
import Navigator from "./Tables/Navigator";

class App extends React.Component {
  state = {
    isLoggedIn: Cookies.get("isLoggedIn") === "true" || false,
    username: Cookies.get("user") || "",
    token: Cookies.get("token") || "",

  };

  getUserToken = (token, user) => {
    this.setState({ username: user, token: "Token " + token });
    Cookies.set("token", "Token " + token);
    Cookies.set("user", user);
  };

  handleLoginClick = (param) => {
    this.setState({ isLoggedIn: param });
  };

	ExitAttempt = () => {
    this.handleLoginClick(false);
    Cookies.remove("token");
    Cookies.remove("user");
    Cookies.set("isLoggedIn", false);
  };

	SetTable = (params) => {
		this.setState({activeTable: params});
	}

  render() {
    return (
      <div className="App">
        {this.state.isLoggedIn ? (
          <div className="MainScrean">
            <div className="Navigator">
            <Navigator switchFunction = {this.SetTable}/>
            </div>
            <div className="DashBoard">
            <div className="login">
              <button className="buttonBack" onClick={this.ExitAttempt}> Выйти </button>
            </div>
            <DashBoard
              token={this.state.token}
							table={this.state.activeTable}
            ></DashBoard>
            </div>
          </div>
        ) : (
          <div >
            <LoginModal
              EntryEvent={this.handleLoginClick}
              getTokenName={this.getUserToken}
            ></LoginModal>
          </div>
        )}
      </div>
    );
  }
}
export default App;
