import React from "react";
import { DataGrid } from "@mui/x-data-grid";

const AUTHORS_URL = "http://dimakirillow.pythonanywhere.com/api/Author";

export default class Record extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.token,
      Authors: [],
    };
  }

  Authors = async () => {
    try {
      const tresponse = await fetch(AUTHORS_URL, {
        headers: {
          Authorization: this.state.token,
          "content-type": "application/json",
        },
      });

      let Authors = await tresponse.json();

      return Authors;
    } catch (err) {
      alert(err);
    }
  };

  updateAllData = async () => {
    try {
      let Authors = await this.Authors();

      this.setState({ Authors: Authors });
    } catch (err) {
      alert(err);
    }
  };

  componentDidMount = async () => {
    this.updateAllData();
  };

  columns = [
    {
      field: "Name",
      headerName: "Имя автора",
      width: 200,
    },
    {
      field: "age",
      headerName: "Возраст",
      width: 200,
    },
    {
      field: "image",
      headerName: "Фото",
      width: 200,
    },
  ];

  render() {
    return (
      <div style={{ height: 550, width: "100%", paddingLeft:"20px",paddingRight:"20px"}}>
        <DataGrid
          getRowId={(row) => row.id}
          rows={this.state.Authors}
          columns={this.columns}
          pageSize={10}
          rowsPerPageOptions={[10]}

        />
        
      </div>
    );
  }
}
