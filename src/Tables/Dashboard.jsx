import React from "react";
import Record from "../Tables/Record";
import Tags from "../Tables/Tags";
import Authors from "../Tables/Authors";
import "../App.css";

export default class DashBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.token,
    };
  }

  render() {
    return (
      <div className="dashboard">
      
          {(() => {
            switch (this.props.table) {
             
              case 2:
                return <Tags token={this.state.token}></Tags>;
              case 3:
                return <Authors token={this.state.token}></Authors>;
              default:
                return <Record token={this.state.token}></Record>;
             
            }
          })()}
        </div>

    );
  }
}
