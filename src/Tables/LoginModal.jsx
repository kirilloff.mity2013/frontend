import React from "react";
import Cookies from "js-cookie";
import "../App.css";

const TOKEN_URL = "//dimakirillow.pythonanywhere.com/api-token-auth/";

export default class LoginModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: "",
      password: "",
      success: true,
    };
  }

  handleChangeLogin(event) {
    this.setState({ login: event.target.value });
  }

  handleChangePass(event) {
    this.setState({ password: event.target.value });
  }

  LoginAttempt = async () => {
    const { EntryEvent, getTokenName } = this.props;
    try {
      const headers = new Headers();
      headers.append("content-type", "application/json");

      const body = JSON.stringify({
        username: this.state.login,
        password: this.state.password,
      });

      const init = {
        method: "POST",
        headers,
        body,
      };

      const response = await fetch(TOKEN_URL, init);
      let token = await response.json();
      if ("token" in token) {
        getTokenName(token.token, this.state.login);
        EntryEvent(true);
        Cookies.set("isLoggedIn", true);
      } else {
        this.setState({ success: false });
      }
    } catch (err) {
      alert(err);
    }
  };

  render() {
    return (
      
      <div className="Login">
   
        <div className="imput" >
            <label >
              Name:
              <input className="input" type="text" value={this.state.login} onChange={(event) => this.handleChangeLogin(event)} required />
            </label>
            <label >
              Password:
              <input className="input" type="password" value={this.state.password} onChange={(event) => this.handleChangePass(event)} required />
            </label>
          </div>
          <div >
            <button className="buttonlogin" onClick={this.LoginAttempt}>Войти</button>
          </div>
          
          {this.state.success ? (
            <div />
          ) : (
            <div>Неправильно введены учетные данные </div>
          )}
          
      </div>
    );
  }
}
