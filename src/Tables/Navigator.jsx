import React from "react";
import "../App.css";

export default class Navigator extends React.Component {
  SwitchTables = (value) => {
    const { switchFunction } = this.props;
    switchFunction(value);
  }

   ButtonArray = [
    {
      text: "Записи",

      value: 1,
    },
    {
      text: "Теги",

      value: 2,
    },
    {
      text: "Авторы",
    
      value: 3,
    },
  ];

  render() {
    return (
      <div className="navigator">
       
        {this.ButtonArray.map((button) => (
          <div>
            <button onClick={() => this.SwitchTables(button.value)} className="button">
       
              {button.text}
            </button>
          </div>
        ))}
      </div>
    );
  }
}
