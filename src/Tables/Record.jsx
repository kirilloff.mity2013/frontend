import React from "react";
import { DataGrid } from "@mui/x-data-grid";

const RECORD_URL = "http://dimakirillow.pythonanywhere.com/api/Record";
const AUTHORS_URL = "http://dimakirillow.pythonanywhere.com/api/Author";
const TAGS_URL = "http://dimakirillow.pythonanywhere.com/api/Tags";

export default class Recrord extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.token,
      data: [],
 
    };
  }
  Authors = async () => {
    try {
      const aresponse = await fetch(AUTHORS_URL, {
        headers: {
          Authorization: this.state.token,
          "content-type": "appdivcation/json",
        },
      });

      let authors = await aresponse.json();
      let authorOptions = [];
      for (let i = 0; i < authors.length; i++) {
        authorOptions.push(authors[i].name);
      }
      this.setState({ authorOptions: authorOptions });
      return authors;
    } catch (err) {
      alert(err);
    }
  };

  Tags = async () => {
    try {
      const tresponse = await fetch(TAGS_URL, {
        headers: {
          Authorization: this.state.token,
          "content-type": "appdivcation/json",
        },
      });

      let tags = await tresponse.json();
      let tagOptions = [];
      for (let i = 0; i < tags.length; i++) {
        tagOptions.push(tags[i].name);
      }
      this.setState({ tagOptions: tagOptions });
      return tags;
    } catch (err) {
      alert(err);
    }
  };

  Record = async () => {
    try {
      const response = await fetch(RECORD_URL, {
        headers: {
          Authorization: this.state.token,
          "content-type": "appdivcation/json",
        },
      });
      let data = await response.json();
      this.setState({ data: data });
      //console.log (data);
     
    } catch (err) {
      alert(err);
    }

  };
  updateAllData = async () => {
    try {
      let date = await this.Record();
      let authors = await this.Authors();
      let tags = await this.Tags();
       this.setState({ date: date, authors: authors, tags: tags });
    } catch (err) {
      alert(err);
    }
  };

  componentDidMount = async () => {
    this.updateAllData();
  };

  columns = [
    {
      field: "title",
      headerName: "Заголовок",
    },
    {
      field: "Author",
      headerName: "Автор",
      width: 200,
    },
    {
      field: "date",
      headerName: "Дата создания",
      type: "dateTime",
      width: 200,
    },
  
    {
      field: "descrition",
      headerName: "Описание",
      width: 200,
    },
  ];
  _onSelect(option) {
    this.setState({ Author: option });
  }
  render() {
    return (
      <div style={{ height: 550, width: "100%", paddingLeft:"20px",paddingRight:"20px"}}>
      <DataGrid
        getRowId={(row) => row.id}
        rows={this.state.data}
        columns={this.columns}
        pageSize={10}
      />
    </div>
    );
  }
}

    