import React from "react";
import { DataGrid } from "@mui/x-data-grid";

const TAGS_URL = "http://dimakirillow.pythonanywhere.com/api/Tags";

export default class Record extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.token,
      tags: [],
    };
  }

  Tags = async () => {
    try {
      const tresponse = await fetch(TAGS_URL, {
        headers: {
          Authorization: this.state.token,
          "content-type": "application/json",
        },
      });

      let tags = await tresponse.json();

      return tags;
    } catch (err) {
      alert(err);
    }
  };

 

  updateAllData = async () => {
    try {
      let tags = await this.Tags();

      this.setState({ tags: tags });
    } catch (err) {
      alert(err);
    }
  };

  componentDidMount = async () => {
    this.updateAllData();
  };

  columns = [
    {
      field: "tag_title",
      headerName: "Заголовок тега",
      width: 200,
    },
    {
      field: "descrition_tags",
      headerName: "Описание тега",
      width: 200,
    },
  ];

  render() {
    return (
      <div style={{ height: 550, width: "100%", paddingLeft:"20px",paddingRight:"20px"}}>
        <DataGrid
          getRowId={(row) => row.id}
          rows={this.state.tags}
          pageSize={10}
          columns={this.columns}
        />
   
      </div>
    );
  }
}
